# aws-cli

Dockerized AWS commandline interface.

```
docker run registry.gitlab.com/openstreetcraft/aws/aws-cli aws help
```
